# Import module
from tkinter import *
from datetime import datetime

due_date = datetime(2030, 10, 21)

timeformat = "%Y"

# Create object
root = Tk()

# Adjust size
root.geometry("200x0-200+0")
root.wm_attributes("-topmost", True)


def update():
    delta = due_date - datetime.now()
    root.title(f"{delta.days}")
    root.after(60000, update)


# root.overrideredirect(True)

# Execute tkinter
def launch():
    root.after(100, update)
    root.mainloop()


if __name__ == "__main__":
    launch()
